var fs = require('fs');
var inspect = require('eyes').inspector();
var crypto = require('crypto');
var querystring = require('querystring');

var keyPairId = 'APKAJT4QYPRS2GVJOZDQ';
var privateKeyPath = './pk-APKAJT4QYPRS2GVJOZDQ.pem';
var privateKeyContents;

fs.realpath(privateKeyPath, function (err, resolvedPath) {
    if (err) 
    	throw err;

    fs.readFile(resolvedPath, function (err, data) {
		if (err) 
			throw err;

		privateKeyContents = data;
		console.log("Private Key Loaded successfully");
    });
});

function _sanityCheckArgs(url, config, callBack) {
	var errors = [];

	if (!url) 
		errors.push('Error: url parameter is required');
	
	if (!config) {
		errors.push('Error: config parameter is required');
		return callBack(errors.join(', '));
	}

	['dateLessThan'].forEach(function (val) {
		if (typeof config[val] === "undefined")
			errors.push('Error: config.' + val + ' is required');
	});

	['dateLessThan', 'dateGreaterThan'].forEach(function (val) {
		if (!config[val]) return;
		if (['[object Date]', '[object Number]'].indexOf(Object.prototype.toString.call(config[val])) === -1)
			errors.push('Error: config.' + val + ' should be a Date object or a Number');
	});

	return callBack (errors.length > 0 ? errors.join(', ') : null);
}

function _buildPolicy(_url, _config, callBack) {
	var _policy = {policyType : 'canned'};

	_config.dateLessThan = Math.round(_config.dateLessThan / 1000);
	var condition = {
		"DateLessThan": {
			"AWS:EpochTime": _config.dateLessThan
		}
	};

	if (_config.dateGreaterThan) {
		_policy.policyType = 'custom';
		condition.DateGreaterThan = {
			"AWS:EpochTime": Math.round(_config.dateGreaterThan / 1000)
		};
	}

	if (_config.ipAddress) {
		_policy.policyType = 'custom';
		condition.IpAddress = {
			"AWS:SourceIp": _config.ipAddress
		};
	}

	_policy.statement = {
			"Statement": [
				{
					"Resource": _url,
					"Condition": condition
				}
			]
		};

	return callBack(null, _policy);
}

module.exports.signUrl = function (url, config, callBack) {
	var argErrors = _sanityCheckArgs(url, config, function (err){
		if (err)
			return callBack (new Error(err));

		config = JSON.parse(JSON.stringify(config), function (key, value) {
			if (['dateLessThan', 'dateGreaterThan'].indexOf(key) !== -1)
				value = new Date(value).getTime();
			return value;
		});

		_buildPolicy(url, config, function(err, policy){
			if (err)
				return callBack(err);
	
			inspect (policy, "Policy");
			var signature = crypto.createSign('RSA-SHA1').update(JSON.stringify(policy.statement)).sign(privateKeyContents, 'base64');
			var query = {
				"Signature": signature.replace(/\+/g, '-').replace(/\=/g, '_').replace(/\//g, '~'),
				"Key-Pair-Id": keyPairId
			};

			if (policy.policyType === 'custom') {
				query.Policy = new Buffer(policy, 'utf8').toString('base64').replace(/\+/g, '-').replace(/\=/g, '_').replace(/\//g, '~');
			} else {
				query.Expires = config.dateLessThan;
			}
			inspect (query, "query");

			var newUrl = url + (url.indexOf('?') === -1 ? '?' : '&') + querystring.stringify(query);
			return callBack(null, newUrl);
		});
	});

}

