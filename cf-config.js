var fs = require('fs');
var inspect = require('eyes').inspector();

if (!fs.existsSync('ip-blacklist.json')) 
	fs.writeFileSync('ip-blacklist.json', JSON.stringify([]));

if (!fs.existsSync('referrer-blacklist.json')) 
	fs.writeFileSync('referrer-blacklist.json', JSON.stringify([]));

if (!fs.existsSync('options.json')) 
	fs.writeFileSync('options.json', JSON.stringify({destination:"http://dqyok8uv2almt.cloudfront.net/"}));

var options = JSON.parse(fs.readFileSync('options.json'));
if (!options.destination)
	options.destination = "/";

if (options.destination.charAt(options.destination.length-1)!=='/')
	options.destination += '/';

var blackLists = {
	ips : {}, 
	referrers : {} 
};

fs.readFile('ip-blacklist.json', function (err, data){
	if (err)
		throw err;

	var ips = JSON.parse(data);
	inspect (ips, "got IPs")
	for (var i=0; i<ips.length; i++) {
		var parts=ips[i].split(".");
		var root = blackLists.ips;
		for (var j=0;j<parts.length;j++) {
			if (!root[parts[j]])
				root[parts[j]]={};
			root = root[parts[j]];
		}
	}
	//inspect (blackLists, "Blacklists");
});

fs.readFile('referrer-blacklist.json', function (err, data){
	if (err)
		throw err;

	var referers = JSON.parse(data);
	inspect (referers, "got Referrers")
	for (var i=0; i<referers.length; i++) {
		var parts=referers[i].split(".");
		var root = blackLists.referrers;
		for (var j=0;j<parts.length;j++) {
			if (!root[parts[j]])
				root[parts[j]]={};
			root = root[parts[j]];
		}
	}
	//inspect (blackLists, "Blacklists");
});

module.exports.checkReferrer = function(referer) {
	var parts=referer.split(".");
	root=blackLists.referrers;
	for (var i=0; i<parts.length; i++) {
		if (!root[parts[i]]) {
			return false;//referer_found = false;
			break;
		}
		root=root[parts[i]];
	}
	return true;
};

module.exports.checkIP = function(ip) {
	parts=ip.split(".");

	root=blackLists.ips;
	for (var i=0; i<parts.length; i++) {
		if (!root[parts[i]]) {
			return false;
			break;
		}
		root=root[parts[i]];
	}
	return true;
};

module.exports.options = options;


