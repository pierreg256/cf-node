var fs = require('fs');
var restify = require('restify');
var inspect = require('eyes').inspector();
var cfSign = require('./cf-sign');
var cfConfig = require('./cf-config');

var server = restify.createServer({
  name: 'Allocine-Media-Provider',
});

 
var loggly = require('loggly')
var config = { subdomain: "pierreg256" };
var client = loggly.createClient(config);

server.get('/', function(req, res, next){
	res.header('Location', '/ping');
	res.send(302);
	next();
});

server.get('/ping', function(req, res, next){
	res.send(200, "OK");
	next();
});

server.head(/^\/(resources)\/(.*)/, function(req, res, next){
	client.log('32ae8942-08ed-4a1e-b74e-8667e9914fa8', 'Head Reaquest for: ' + req.params[1]);
	res.status(204);
	res.end();
	next();
});

server.get(/^\/(resources)\/(.*)/, function(req, res, next){
	var res_url = req.params[1];
	var res_referer = req.header('referer');
	var res_ip = req.connection.remoteAddress;

	if (res_referer && cfConfig.checkReferrer(res_referer)) {
		res.send(403, {message:"Referrer blacklisted"});
		client.log('32ae8942-08ed-4a1e-b74e-8667e9914fa8', 'Referrer ' + res_referer + ' Blacklisted for URL: ' + req.params[1]);
		next();
	} else {
		if (cfConfig.checkIP(res_ip)) {
			res.send(403, {message:"IP blacklisted"});
			client.log('32ae8942-08ed-4a1e-b74e-8667e9914fa8', 'IP ' + res_ip + ' Blacklisted for URL: ' + req.params[1]);
			next();
		} else {
			var dateLessThan = new Date();
			dateLessThan.setHours(dateLessThan.getHours()+1);

			cfSign.signUrl(cfConfig.options.destination+req.params[1], {"dateLessThan": dateLessThan}, function(err, signedURL){
				if (err)
					res.send(500, err);
				else {
					res.header('Location', signedURL);
					res.send(301);
					client.log('32ae8942-08ed-4a1e-b74e-8667e9914fa8', 'Resource accepted for : {' + res_ip + ', ' + req.params[1] + "}");
				}
				next();
			});
		}
	}
});

server.on('after', restify.auditLogger({
        log: require('bunyan').createLogger({
                name: server.name
        })
}))


server.listen(80, function () {
    console.log('server listening at %s', server.url);
    client.log('32ae8942-08ed-4a1e-b74e-8667e9914fa8', 'server listening at %s' + server.url)

});